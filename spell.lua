local function makeSpell(name, uses)
  local Spell = { name = name, 
                  maxUses = uses, 
                  remainingUses = uses }
  Spell.__index = Spell
  function Spell:new()
    local o = {}
    setmetatable(o, Spell)
    return o
  end
  function Spell:cast(caster, victim)
    if self.maxUses then
      self.remainingUses = self.remainingUses - 1
    end
    return self:_cast(caster, victim)
  end
  return Spell
end

Absorb = makeSpell("Absorb")
function Absorb:_cast(caster, victim)
  local spellPool = victim.spells
  local spellIndex = math.ceil(math.random(#spellPool))
  local spell = spellPool[spellIndex]:new()
  table.insert(caster.spells, spellPool[spellIndex]:new())
  return caster.name .. " absorbed " .. spell.name
end

Punch = makeSpell("Punch", 20)
function Punch:_cast(caster, victim)
  local boostedDamage = caster:getPhysicalDamage(10)
  local actualDamage = victim:inflictPhysicalDamage(boostedDamage)
  return caster.name .. " inflicted " .. actualDamage .. " damage!"
end

Heal = makeSpell("Heal", 5)
function Heal:_cast(caster, victim)
  local boostedDamage = caster:getMagicDamage(25)
  caster:heal(boostedDamage)
  return caster.name .. " healed " .. boostedDamage .. " health!"
end

