Character = { maxHealth = 100, 
              currentHealth = 100,
              selectedSpell = 1,
              -- Character base stats
              physicalDamageBonus = 0,
              physicalDamageArmor = 0,
              magicDamageBonus = 0,
              -- Character per fight stats
              fightDamageBonus = 0 }
Character.__index = Character
function Character:new(o, name, health, spells)
  local o = o or {}
  o.spells = spells
  o.name = name
  o.maxHealth = health
  o.currentHealth = health
  setmetatable(o, Character_mt)
  return o
end

function Character:prepareForFight()
  self.selectedSpell = 1
  self.fightDamageBonus = 0
end

function Character:pruneSpells()
  local newSpells = {}
  for i, spell in ipairs(self.spells) do
    if not spell.maxUses or spell.remainingUses > 0 then
      table.insert(newSpells, spell)
    end
  end
  self.spells = newSpells
  if self.selectedSpell > #newSpells then
    self.selectedSpell = #newSpells
  end
end

function Character:getPhysicalDamage(baseDamage)
  return baseDamage * (1 + self.physicalDamageBonus + self.fightDamageBonus)
end

function Character:inflictPhysicalDamage(damage)
  local mitigatedDamage = damage * (1 - self.physicalDamageArmor)
  self:inflictPureDamage(mitigatedDamage)
  return mitigatedDamage
end

function Character:getMagicDamage(baseDamage)
  return baseDamage * (1 + self.magicDamageBonus)
end

function Character:heal(health)
  self.currentHealth = self.currentHealth + health
  if self.currentHealth > self.maxHealth then
    self.currentHealth = self.maxHealth
  end
end

function Character:inflictPureDamage(damage)
  self.currentHealth = self.currentHealth - damage
  if self.currentHealth < 0 then
    self.currentHealth = 0
  end
end
