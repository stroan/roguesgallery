-- Debug deps
inspect = require 'inspect'

-- Game deps
require 'fight'
require 'map'
require 'player'

--------------------------------------------------------------------------------
-- Main callback functions
--------------------------------------------------------------------------------

local player
local map
local fight

local function resetGame()
  player = Player:new()
  map = Map:new()
  fight = nil
end

function love.load()
  resetGame()
end

function love.draw()
  if fight == nil then
    map:draw()
  else
    fight:draw()
  end
end

function love.update(dt)
  if not (fight == nil) then
    finished, won = fight:update(dt)
    if finished and won then
      fight = nil
    elseif finished and not won then
      resetGame()
    end
  end
end

function love.keypressed(key, unicode)
  if key == 'escape' then
    love.event.quit()
  end

  if fight == nil then
    if map:handleKey(key) then
      -- Time to start a new fight. Hooah.
      fight = Fight:new(player)
    end
  else
    -- Currently in a fight
    fight:handleKey(key)
  end
end
