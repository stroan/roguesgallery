require 'badguy'

Fight = {}
Fight.__index = Fight
function Fight:new(player)
  player:prepareForFight()
  local o = { player = player,
              badGuy = BadGuy:new(),
              isPlayersTurn = true,
              alertText = "A wild prison guard appeared!",
              alertTimeout = 2 }
  setmetatable(o, self)
  return o
end

local function drawFighter(name, color, x, y)
  love.graphics.setColor(color)
  love.graphics.rectangle("fill", x, y, 100, 200)

  love.graphics.setColor(255,255,255,255)
  love.graphics.print(name, x + 10 , y + 100)
end

local function drawControlBox(x, y, character, active)
  -- Surrounding box
  if active then
    love.graphics.setLineWidth(3)
  else
    love.graphics.setLineWidth(1)
  end
  love.graphics.push()
  love.graphics.translate(x, y)
  love.graphics.setColor(128,128,128,255)
  love.graphics.rectangle("line", 0, 0, 150, 300)

  -- Health bar
  love.graphics.setColor(0, 128, 0, 255)
  love.graphics.print("Health: " .. character.currentHealth .. "/" .. character.maxHealth, 5, 5)
  love.graphics.rectangle("line", 5, 25, 140, 20)
  love.graphics.rectangle("fill", 7, 27, 136 * (character.currentHealth / character.maxHealth), 16)

  -- Actions
 
  -- There's probably a less awful way to do this...
  local firstAction = character.selectedSpell - 1
  if firstAction < 1 then
    firstAction = 1
  end
  if firstAction + 3 > #(character.spells) then
    firstAction = #(character.spells) - 2
  end
  if firstAction < 1 then
    firstAction = 1
  end

  love.graphics.setColor(255,255,255,255)
  love.graphics.print("Abilities: " .. character.selectedSpell .. "/" .. (#character.spells), 5, 60)
  for i, spell in ipairs(character.spells) do
    if i >= firstAction and i - firstAction < 3 then
      if i == character.selectedSpell and active then
        love.graphics.setLineWidth(2)
      else
        love.graphics.setLineWidth(1)
      end
      local yOffset = 75 + ((i - firstAction) * 60)
      love.graphics.rectangle("line", 5, yOffset, 140, 50)
      love.graphics.print(spell.name, 10, yOffset + 20)
      if spell.maxUses then
        love.graphics.print("" .. spell.remainingUses, 110, yOffset + 20)
      end
    end
  end

  love.graphics.pop()
end

function Fight:draw()
  -- TODO: Centre camera so that the 800x600 interface
  -- sits in the middle of whatever the actual screen
  -- size is.

  drawFighter("YOU", {0, 0, 255, 255}, 200, 275)
  drawFighter("ENEMY", {255, 0, 0, 255}, 500, 125)

  drawControlBox(0, 275, self.player, self.isPlayersTurn)
  drawControlBox(650, 25, self.badGuy, not self.isPlayersTurn)

  if self.alertText then
    love.graphics.setColor(0,0,0,255)
    love.graphics.rectangle("fill", 200, 200, 400, 200)
    love.graphics.setColor(255,255,255,255)
    love.graphics.rectangle("line", 200, 200, 400, 200)
    love.graphics.print(self.alertText, 250, 250)
  end
end

function Fight:isOver()
  if self.player.currentHealth == 0 then
    return true, false
  elseif self.badGuy.currentHealth == 0 then
    return true, true
  else
    return false
  end
end

function Fight:update(dt)
  if self.alertText then
    self.alertTimeout = self.alertTimeout - dt
    if self.alertTimeout < 0 then
      self.alertTimeout = nil
      self.alertText = nil
    end
    return
  end

  if self.isPlayersTurn then
    return self:isOver()
  end

  if self.badGuy:update(dt) then
    local spell = self.badGuy.spells[self.badGuy.selectedSpell]
    self:setAlert(spell:cast(self.badGuy, self.player))
    self.badGuy:pruneSpells()
    self.isPlayersTurn = true
    return
  end

  return self:isOver()
end

function Fight:handleKey(key)
  if self.alertText then
    self:dismissAlert()
    return
  end

  if not self.isPlayersTurn then
    return
  end

  local character = self.player
  if key == 'down' and character.selectedSpell < #(character.spells) then
    character.selectedSpell = character.selectedSpell + 1
  elseif key == 'up' and character.selectedSpell > 1 then
    character.selectedSpell = character.selectedSpell - 1
  elseif key == ' ' or key == 'return' then
    local spell = character.spells[character.selectedSpell]
    self:setAlert(spell:cast(character, self.badGuy))
    character:pruneSpells()
    self.badGuy:startTurn()
    self.isPlayersTurn = false
  end
end

function Fight:setAlert(text)
  local over, win = self:isOver()
  if over and win then
    text = text .. " Victory!"
  elseif over and not win then
    text = text .. " You have died."
  end
  self.alertText = text
  self.alertTimeout = 2
end

function Fight:dismissAlert()
  self.alertText = nil
  self.alertTimeout = nil
end
