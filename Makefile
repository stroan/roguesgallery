multidist: dist/RoguesGallery.love dist/RoguesGallery-win64.zip dist/RoguesGallery-win32.zip dist/RoguesGallery-linux.tar.gz
	echo Done

dist/RoguesGallery-linux.tar.gz: dist/RoguesGallery.love
	cp `which love` dist/build/love
	cat dist/build/love dist/RoguesGallery.love > dist/build/RoguesGallery
	chmod +x dist/build/RoguesGallery
	tar czvf dist/RoguesGallery-linux.tar.gz dist/build/RoguesGallery

dist/RoguesGallery-win32.zip: dist/RoguesGallery.love dist/build/love-0.8.0-win-x86/love.exe
	rm -fr dist/build/RoguesGallery-win64
	cp -r dist/build/love-0.8.0-win-x86 dist/build/RoguesGallery-win32
	cat dist/build/RoguesGallery-win32/love.exe dist/RoguesGallery.love > dist/build/RoguesGallery-win32/RoguesGallery.exe
	rm dist/build/RoguesGallery-win32/love.exe
	zip -r dist/RoguesGallery-win32.zip dist/build/RoguesGallery-win32

dist/RoguesGallery-win64.zip: dist/RoguesGallery.love dist/build/love-0.8.0-win-x64/love.exe
	rm -fr dist/build/RoguesGallery-win64
	cp -r dist/build/love-0.8.0-win-x64 dist/build/RoguesGallery-win64
	cat dist/build/RoguesGallery-win64/love.exe dist/RoguesGallery.love > dist/build/RoguesGallery-win64/RoguesGallery.exe
	rm dist/build/RoguesGallery-win64/love.exe
	zip -r dist/RoguesGallery-win64.zip dist/build/RoguesGallery-win64

dist/build/love-0.8.0-win-x64/love.exe: dist
	rm -fr dist/build/love-0.8.0-win-x64
	curl -L https://bitbucket.org/rude/love/downloads/love-0.8.0-win-x64.zip > dist/build/love.win64.zip
	unzip dist/build/love.win64.zip -d dist/build

dist/build/love-0.8.0-win-x86/love.exe: dist
	rm -fr dist/build/love-0.8.0-win-x86
	curl -L https://bitbucket.org/rude/love/downloads/love-0.8.0-win-x86.zip > dist/build/love.win32.zip
	unzip dist/build/love.win32.zip -d dist/build

dist/RoguesGallery.love: main.lua dist
	zip dist/RoguesGallery.love *.lua

dist:
	mkdir -p dist/build

clean:
	rm -fr dist
