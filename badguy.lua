require 'character'
require 'spell'

BadGuy = { nextSpell = nil,
           timeSinceUIAction = nil }
BadGuy.__index = BadGuy
setmetatable(BadGuy, Character)
function BadGuy:new()
  local spells = { Punch:new(), Heal:new() }
  local o = {}
  Character:new(o, "BadGuy", 40, spells)
  setmetatable(o, self)
  return o
end

function BadGuy:update(dt)
  self.timeSinceUIAction = self.timeSinceUIAction + dt
  if self.timeSinceUIAction > 1 then
    self.timeSinceUIAction = 0
    if self.selectedSpell == self.nextSpell then
      return true
    elseif self.selectedSpell < self.nextSpell then
      self.selectedSpell = self.selectedSpell + 1
    elseif self.selectedSpell > self.nextSpell then
      self.selectedSpell = self.selectedSpell - 1
    end
  end
end

function BadGuy:startTurn()
  self.nextSpell = math.ceil(math.random() * #self.spells)
  self.timeSinceUIAction = 0
end
