-- Map Tiles Codes
-- 0 - Empty
-- 1 - Unexplored room
-- 2 - Explored room
-- 3 - Vertical hall
-- 4 - Horizontal hall
-- 5 - Crossroads

local HALF_TILE_SIZE = 40
local TILE_SIZE = 2 * HALF_TILE_SIZE
local HALF_CORRIDOR_SIZE = 20
local CORRIDOR_SIZE = 2 * HALF_CORRIDOR_SIZE
local HALF_PLAYER_SIZE = 10
local PLAYER_SIZE = 2 * HALF_PLAYER_SIZE

local drawFunctions = 
  { [0] = (function () end),
    [1] = (function ()
      love.graphics.setColor(255,128,128,255)
      love.graphics.rectangle("fill", -HALF_TILE_SIZE, -HALF_TILE_SIZE, 
                                      TILE_SIZE, TILE_SIZE)
    end),
    [2] = (function ()
      love.graphics.setColor(128,255,128,255)
      love.graphics.rectangle("fill", -HALF_TILE_SIZE, -HALF_TILE_SIZE, 
                                      TILE_SIZE, TILE_SIZE)
    end),
    [3] = (function ()
      love.graphics.setColor(128,128,128,255)
      love.graphics.rectangle("fill", -HALF_CORRIDOR_SIZE, -HALF_TILE_SIZE, 
                                      CORRIDOR_SIZE, TILE_SIZE)
    end), 
    [4] = (function ()
      love.graphics.setColor(128,128,128,255)
      love.graphics.rectangle("fill", -HALF_TILE_SIZE, -HALF_CORRIDOR_SIZE, 
                                      TILE_SIZE, CORRIDOR_SIZE)
    end) }

local function drawPerson()
  love.graphics.setColor(0, 0, 255, 255)
  love.graphics.rectangle("fill", -HALF_PLAYER_SIZE, -HALF_PLAYER_SIZE, 
                                  PLAYER_SIZE, PLAYER_SIZE)
end

local function drawBoss()
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle("fill", -HALF_PLAYER_SIZE, -HALF_PLAYER_SIZE, 
                                  PLAYER_SIZE, PLAYER_SIZE)
end

Map = {}
Map.__index = Map
function Map:new()
  local map = { { 2, 4, 1, 0, 0 },
                { 3, 0, 3, 0, 0 },
                { 1, 4, 1, 4, 1 } }
  local bossRoom = { x = 5, y = 3 }
  local playerPos = { x = 1, y = 1 }

  local o = { map = map,
              bossRoom = bossRoom,
              rows = #map,
              cols = #(map[1]),
              playerPos = playerPos }
  setmetatable(o, self)
  return o
end

function Map:draw()
  local windowWidth = love.graphics.getWidth()
  local windowHeight = love.graphics.getHeight()

  love.graphics.push()
  love.graphics.translate(windowWidth / 2, windowHeight / 2)

  love.graphics.translate((self.playerPos.x - 1) * -TILE_SIZE, (self.playerPos.y - 1) * -TILE_SIZE)

  for y, row in ipairs(self.map) do
    for x, col in ipairs(row) do
      love.graphics.push()
      love.graphics.translate(TILE_SIZE * (x - 1), TILE_SIZE * (y - 1))
      drawFunctions[col]()

      if x == self.playerPos.x and y == self.playerPos.y then
        drawPerson()
      end

      if x == self.bossRoom.x and y == self.bossRoom.y then
        drawBoss()
      end

      love.graphics.pop()
    end
  end

  love.graphics.pop()
end

function Map:handleKey(key)
  if key == 'right' then
    return self:tryMoveToCell(self.playerPos.x + 1, self.playerPos.y)
  elseif key == 'left' then
    return self:tryMoveToCell(self.playerPos.x - 1, self.playerPos.y)
  elseif key == 'down' then
    return self:tryMoveToCell(self.playerPos.x, self.playerPos.y + 1)
  elseif key == 'up' then
    return self:tryMoveToCell(self.playerPos.x, self.playerPos.y - 1)
  end
end

function Map:tryMoveToCell(x, y)
  if x < 1 or y < 1 or x > self.cols or y > self.rows then
    return
  end

  local destCell = self.map[y][x]
  if destCell > 0 then
    self.playerPos.x = x
    self.playerPos.y = y
  end

  if destCell == 1 then
    self.map[y][x] = 2
    return true
  end
end
