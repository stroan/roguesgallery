require 'character'
require 'spell'

Player = {}
Player.__index = Player
setmetatable(Player, Character)
function Player:new()
  local spells = { Absorb:new() }
  local o = {}
  Character:new(o, "You", 100, spells)
  setmetatable(o, self)
  return o
end
